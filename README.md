# kotlin-eliza

An implementation of ELIZA in Kotlin.

## References
J. Weizenbaum, ["ELIZA - A Computer Program For the Study of Natural Language Communication Between Man And Machine,"](http://www.cse.buffalo.edu/~rapaport/572/S02/weizenbaum.eliza.1966.pdf) Communications of the ACM, Vol 9, No 1, January 1966

## Acknowledgements
The content of the dialogue fragments is adapted from [eliza.py](https://github.com/jezhiggins/eliza.py), an implementation of Eliza in Python by Jez Higgins based on work by Joe Strout with some updates by Jeff Epler.
