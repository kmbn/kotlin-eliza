// An implementation of ELIZA in Kotlin

import kotlin.random.Random
import kotlin.system.exitProcess


data class DialogueFragment(
    val regex: Regex,
    val replyTemplates: Array<String>
)


val dialogueFragments = listOf(
    DialogueFragment(
        "I need (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Why do you need %s?",
            "Would it really help you to get %s?",
            "Are you sure you need %s?"
        )
    ),
    DialogueFragment(
        "I am (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Did you come to me because you are %s?",
            "How long have you been %s?",
            "How do you feel about being %s?"
        )
    ),
    DialogueFragment(
        "Why don\\'?t you ([^\\?]*)\\??".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Do you really think I don't %s?",
            "Perhaps eventually I will %s.",
            "Do you really want me to %s?"
        )
    ),
    DialogueFragment(
        "Why can\\'?t I ([^\\?]*)\\??".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Do you think you should be able to %s?",
            "If you could %s, what would you do?",
            "I don't know -- why can't you %s?",
            "Have you really tried?"
        )
    ),
    DialogueFragment(
        "I can\\'?t (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "How do you know you can't %s?",
            "Perhaps you could %s if you tried.",
            "What would it take for you to %s?"
        )
    ),
    DialogueFragment(
        "I\\'?m (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "How does being %s make you feel?",
            "Do you enjoy being %s?",
            "Why do you tell me you're %s?",
            "Why do you think you're %s?"
        )
    ),
    DialogueFragment(
        "Are you ([^\\?]*)\\??".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Why does it matter whether I am %s?",
            "Would you prefer it if I were not %s?",
            "Perhaps you believe I am %s.",
            "I may be %s -- what do you think?"
        )
    ),
    DialogueFragment(
        "What (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Why do you ask?",
            "How would an answer to that help you?",
            "What do you think?"
        )
    ),
    DialogueFragment(
        "How (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "How do you suppose?",
            "Perhaps you can answer your own question.",
            "What is it you're really asking?"
        )
    ),
    DialogueFragment(
        "Because (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Is that the real reason?",
            "What other reasons come to mind?",
            "Does that reason apply to anything else?",
            "If %s, what else must be true?"
        )
    ),
    DialogueFragment(
        "(.*) sorry (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "There are many times when no apology is needed.",
            "What feelings do you have when you apologize?"
        )
    ),
    DialogueFragment(
        "Hello(.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Hello… I'm glad you could drop by today.",
            "Hi there… how are you today?",
            "Hello, how are you feeling today?"
        )
    ),
    DialogueFragment(
        "I think (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Do you doubt %s?",
            "Do you really think so?",
            "But you're not sure %s?"
        )
    ),
    DialogueFragment(
        "(.*) friend (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Tell me more about your friends.",
            "When you think of a friend, what comes to mind?",
            "Why don't you tell me about a childhood friend?"
        )
    ),
    DialogueFragment(
        "(Yes)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "You seem quite sure.",
            "OK, but can you elaborate a bit?"
        )
    ),
    DialogueFragment(
        "(.*) computer(.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Are you really talking about me?",
            "Does it seem strange to talk to a computer?",
            "How do computers make you feel?",
            "Do you feel threatened by computers?"
        )
    ),
    DialogueFragment(
        "Is it (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Do you think it is %s?",
            "Perhaps it's %s -- what do you think?",
            "If it were %s, what would you do?",
            "It could well be that %s."
        )
    ),
    DialogueFragment(
        "It is (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "You seem very certain.",
            "If I told you that it probably isn't %s, what would you feel?"
        )
    ),
    DialogueFragment(
        "Can you ([^\\?]*)\\??".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "What makes you think I can't %s?",
            "If I could %s, then what?",
            "Why do you ask if I can %s?"
        )
    ),
    DialogueFragment(
        "Can I ([^\\?]*)\\??".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Perhaps you don't want to %s.",
            "Do you want to be able to %s?",
            "If you could %s, would you?"
        )
    ),
    DialogueFragment(
        "You are (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Why do you think I am %s?",
            "Does it please you to think that I'm %s?",
            "Perhaps you would like me to be %s.",
            "Perhaps you're really talking about yourself?"
        )
    ),
    DialogueFragment(
        "You\\'?re (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Why do you say I am %s?",
            "Why do you think I am %s?",
            "Are we talking about you, or me?"
        )
    ),
    DialogueFragment(
        "I don'?t (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Don't you really %s?",
            "Why don't you %s?",
            "Do you want to %s?"
        )
    ),
    DialogueFragment(
        "I feel (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Good, tell me more about these feelings.",
            "Do you often feel %s?",
            "When do you usually feel %s?",
            "When you feel %s, what do you do?"
        )
    ),
    DialogueFragment(
        "I have (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Why do you tell me that you've %s?",
            "Have you really %s?",
            "Now that you have %s, what will you do next?"
        )
    ),
    DialogueFragment(
        "I would (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Could you explain why you would %s?",
            "Why would you %s?",
            "Who else knows that you would %s?"
        )
    ),
    DialogueFragment(
        "Is there (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Do you think there is %s?",
            "It's likely that there is %s.",
            "Would you like there to be %s?"
        )
    ),
    DialogueFragment(
        "My (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "I see, your %s.",
            "Why do you say that your %s?",
            "When your %s, how do you feel?"
        )
    ),
    DialogueFragment(
        "You (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "We should be discussing you, not me.",
            "Why do you say that about me?",
            "Why do you care whether I %s?"
        )
    ),
    DialogueFragment(
        "Why (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Why don't you tell me the reason why %s?",
            "Why do you think %s?"
        )
    ),
    DialogueFragment(
        "I want (.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "What would it mean to you if you got %s?",
            "Why do you want %s?",
            "What would you do if you got %s?",
            "If you got %s, then what would you do?"
        )
    ),
    DialogueFragment(
        "(.*) mother(.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Tell me more about your mother.",
            "What was your relationship with your mother like?",
            "How do you feel about your mother?",
            "How does this relate to your feelings today?",
            "Good family relations are important."
        )
    ),
    DialogueFragment(
        "(.*) father(.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Tell me more about your father.",
            "How did your father make you feel?",
            "How do you feel about your father?",
            "Does your relationship with your father relate to your feelings today?",
            "Do you have trouble showing affection with your family?"
        )
    ),
    DialogueFragment(
        "(.*) child(.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Did you have close friends as a child?",
            "What is your favorite childhood memory?",
            "Do you remember any dreams or nightmares from childhood?",
            "Did the other children sometimes tease you?",
            "How do you think your childhood experiences relate to your feelings today?"
        )
    ),
    DialogueFragment(
        "(.*)\\?".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Why do you ask that?",
            "Please consider whether you can answer your own question.",
            "Perhaps the answer lies within yourself?",
            "Why don't you tell me?"
        )
    ),
    DialogueFragment(
        "(.*)".toRegex(RegexOption.IGNORE_CASE),
        arrayOf(
            "Please tell me more.",
            "Let's change focus a bit… Tell me about your family.",
            "Can you elaborate on that?",
            "Why do you say that %s?",
            "I see.",
            "Very interesting.",
            "%s.",
            "I see. And what does that tell you?",
            "How does that make you feel?",
            "How do you feel when you say that?"
        )
    )
)


val reflections = mapOf(
    "am" to "are",
    "was" to "were",
    "i" to "you",
    "i'd" to "you would",
    "i've" to "you have",
    "i'll" to "you will",
    "my" to "your",
    "are" to "am",
    "you've" to "I have",
    "you'll" to "I will",
    "your" to "my",
    "yours" to "mine",
    "you" to "me",
    "me" to "you"
)


fun Array<String>.randomString(): String? {
    return this.getOrNull(Random.nextInt(0, this.size))
}


fun ask(question: String): String {
    println(question)
    print("> ")
    return readLine()!!
}


fun converse(_input: String) {
    var input = _input
    while (true) {
        when (input) {
            ":quit" -> sayGoodbye()
            else -> {
                input = reply(input)
            }
        }
    }
}


fun sayGoodbye() {
    println("I hope our conversation was helpful. Goodbye for now.")
    exitProcess(0)
}


fun reply(input: String): String {
    for (dialogueFragment in dialogueFragments) {
        val match = dialogueFragment.regex.find(input)
        if (match != null) {
            val (matchedTopic) = match.destructured
            val reflectedTopic = reflectTopic(matchedTopic)
            val replyTemplate = dialogueFragment.replyTemplates.randomString()!!
            return ask(replyTemplate.format(reflectedTopic))
        }
    }
    return ask("I'm sorry, I don't think I understood you. Could you please repeat that?")
}


fun reflectTopic(matchedTopic: String): String {
    val wordList = matchedTopic.split(" ")
    val reflectedWordList = wordList.map { word -> reflections.getOrDefault(word.toLowerCase(), word) }
    return reflectedWordList.joinToString(" ")
}


fun main() {
    println(
        """
        Welcome to Eliza KT.
        Our conversation will be completely confidential.
        Enter `:quit` at any time to quit.
        """
    )
    val conversationStarter = ask("Do you want to talk?")
    converse(conversationStarter)
}
